<?php
/**
 * @file
 * sc_drupalreference.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function sc_drupalreference_field_default_fields() {
  $fields = array();

  // Exported field: 'node-sc_resource_external-field_drupal_project'
  $fields['node-sc_resource_external-field_drupal_project'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_drupal_project',
      'foreign keys' => array(
        'tid' => array(
          'columns' => array(
            'tid' => 'tid',
          ),
          'table' => 'taxonomy_term_data',
        ),
      ),
      'indexes' => array(
        'tid' => array(
          0 => 'tid',
        ),
      ),
      'module' => 'taxonomy',
      'settings' => array(
        'allowed_values' => array(
          0 => array(
            'vocabulary' => 'drupal_full_projects',
            'parent' => '0',
          ),
        ),
      ),
      'translatable' => '0',
      'type' => 'taxonomy_term_reference',
    ),
    'field_instance' => array(
      'bundle' => 'sc_resource_external',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Add any Drupal projects relevant for this resource.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'taxonomy',
          'settings' => array(),
          'type' => 'taxonomy_term_reference_link',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_drupal_project',
      'label' => 'Drupal project',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'web_taxonomy',
        'settings' => array(
          'autocomplete_path' => 'web_taxonomy/autocomplete',
          'size' => 60,
        ),
        'type' => 'web_taxonomy_autocomplete',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'taxonomy_term-drupal_full_projects-web_tid'
  $fields['taxonomy_term-drupal_full_projects-web_tid'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'taxonomy_term',
      ),
      'field_name' => 'web_tid',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(
        'max_length' => 2080,
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'drupal_full_projects',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'The URL or other universal identifier for this term.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'taxonomy_term',
      'field_name' => 'web_tid',
      'label' => 'Web Term ID',
      'required' => FALSE,
      'settings' => array(
        'text_processing' => 0,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'module' => 'text',
        'settings' => array(
          'size' => 60,
        ),
        'type' => 'text_textfield',
        'weight' => 1,
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Add any Drupal projects relevant for this resource.');
  t('Drupal project');
  t('The URL or other universal identifier for this term.');
  t('Web Term ID');

  return $fields;
}
