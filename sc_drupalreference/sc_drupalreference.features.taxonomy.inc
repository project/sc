<?php
/**
 * @file
 * sc_drupalreference.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function sc_drupalreference_taxonomy_default_vocabularies() {
  return array(
    'drupal_full_projects' => array(
      'name' => 'Drupal Full Projects - Web Taxonomy',
      'machine_name' => 'drupal_full_projects',
      'description' => 'Connects to the external Drupal Full Projects vocabulary.',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'microdata' => array(
        '#attributes' => array(
          'itemscope' => '',
        ),
        'comment_body' => array(
          '#attributes' => array(),
        ),
        'body' => array(
          '#attributes' => array(),
        ),
        'field_tags' => array(
          '#attributes' => array(),
        ),
        'field_image' => array(
          '#attributes' => array(),
        ),
        'sc_topic_requires' => array(
          '#attributes' => array(),
        ),
        'sc_topic_description' => array(
          '#attributes' => array(),
        ),
        'sc_topic_isapartof' => array(
          '#attributes' => array(),
        ),
        'sc_topic_isapartof_strength' => array(
          '#attributes' => array(),
        ),
        'sc_topic_requires_strength' => array(
          '#attributes' => array(),
        ),
        'sc_topicrelation_strength' => array(
          '#attributes' => array(),
        ),
        'sc_externalresource_link' => array(
          '#attributes' => array(),
        ),
        'sc_resource_description' => array(
          '#attributes' => array(),
        ),
        'sc_resource_topics' => array(
          '#attributes' => array(),
        ),
        'sc_resource_vote_clear' => array(
          '#attributes' => array(),
        ),
        'sc_resource_vote_correct' => array(
          '#attributes' => array(),
        ),
        'sc_resource_vote_entertaining' => array(
          '#attributes' => array(),
        ),
        'web_tid' => array(
          '#attributes' => array(),
        ),
        'field_drupal_project' => array(
          '#attributes' => array(),
        ),
      ),
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
