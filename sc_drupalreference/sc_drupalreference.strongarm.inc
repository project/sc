<?php
/**
 * @file
 * sc_drupalreference.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function sc_drupalreference_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_enabled_vocabularies';
  $strongarm->value = array(
    'schema_org' => 'schema_org',
  );
  $export['microdata_enabled_vocabularies'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_sc_resource_external';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_sc_resource_external'] = $strongarm;

  return $export;
}
