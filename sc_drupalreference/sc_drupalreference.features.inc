<?php
/**
 * @file
 * sc_drupalreference.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sc_drupalreference_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_microdata_mappings_defaults().
 */
function sc_drupalreference_microdata_mappings_defaults() {
  $microdata_mappings = array();

  // Exported Microdata mapping: drupal_full_projects
  $microdata_mappings['taxonomy_term']['drupal_full_projects'] = array(
    'web_tid' => array(
      '#itemprop' => array(),
      '#itemtype' => array(),
      '#value_type' => 'text',
    ),
    '#value_type' => 'item',
    '#itemtype' => array(
      0 => 'http://schema.org/SoftwareApplication',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[term:web_tid]',
    'description' => array(
      '#itemprop' => '',
    ),
    'url' => array(
      '#itemprop' => '[term:url]',
    ),
  );

  // Exported Microdata mapping: sc_resource_external
  $microdata_mappings['node']['sc_resource_external'] = array(
    'sc_externalresource_link' => array(
      '#itemprop' => array(),
      '#itemtype' => array(),
      'url' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
      ),
    ),
    'sc_resource_description' => array(
      '#itemprop' => array(),
      '#itemtype' => array(),
      'value' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
        '#value_type' => 'text',
      ),
      'format' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
      ),
      '#value_type' => 'text',
    ),
    'sc_resource_topics' => array(
      '#itemprop' => array(),
      '#itemtype' => array(),
    ),
    'sc_resource_vote_clear' => array(
      '#itemprop' => array(),
      '#itemtype' => array(),
      'average_rating' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
        '#value_type' => 'text',
      ),
      'user_rating' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
        '#value_type' => 'text',
      ),
      '#value_type' => NULL,
    ),
    'sc_resource_vote_correct' => array(
      '#itemprop' => array(),
      '#itemtype' => array(),
      'average_rating' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
        '#value_type' => 'text',
      ),
      'user_rating' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
        '#value_type' => 'text',
      ),
      '#value_type' => NULL,
    ),
    'sc_resource_vote_entertaining' => array(
      '#itemprop' => array(),
      '#itemtype' => array(),
      'average_rating' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
        '#value_type' => 'text',
      ),
      'user_rating' => array(
        '#itemprop' => array(),
        '#itemtype' => array(),
        '#value_type' => 'text',
      ),
      '#value_type' => NULL,
    ),
    'field_drupal_project' => array(
      '#itemprop' => array(
        0 => 'about',
      ),
      '#itemtype' => array(),
    ),
    '#value_type' => 'item',
    '#itemtype' => array(
      0 => 'http://schema.org/Article',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[node:url]',
  );

  return $microdata_mappings;
}
