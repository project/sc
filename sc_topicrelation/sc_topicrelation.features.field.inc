<?php
/**
 * @file
 * sc_topicrelation.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function sc_topicrelation_field_default_fields() {
  $fields = array();

  // Exported field: 'node-sc_topic-sc_topic_isapartof_strength'
  $fields['node-sc_topic-sc_topic_isapartof_strength'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'sc_topic_isapartof_strength',
      'field_permissions' => array(
        'type' => '2',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'number',
      'settings' => array(
        'decimal_separator' => '.',
      ),
      'translatable' => '0',
      'type' => 'number_float',
    ),
    'field_instance' => array(
      'bundle' => 'sc_topic',
      'default_value' => array(
        0 => array(
          'value' => '0',
        ),
      ),
      'deleted' => '0',
      'description' => 'This field should not be visible when editing topics – it is populated and changed by code to reflect votes on this topic\'s relations.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'sc_topic_isapartof_strength',
      'label' => 'Inclusion strength',
      'required' => 1,
      'settings' => array(
        'max' => '',
        'min' => '',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => '-1',
      ),
    ),
  );

  // Exported field: 'node-sc_topic-sc_topic_requires_strength'
  $fields['node-sc_topic-sc_topic_requires_strength'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'sc_topic_requires_strength',
      'field_permissions' => array(
        'type' => '2',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'number',
      'settings' => array(
        'decimal_separator' => '.',
      ),
      'translatable' => '0',
      'type' => 'number_float',
    ),
    'field_instance' => array(
      'bundle' => 'sc_topic',
      'default_value' => array(
        0 => array(
          'value' => '0',
        ),
      ),
      'deleted' => '0',
      'description' => 'This field should not be visible when editing topics – it is populated and changed by code to reflect votes on this topic\'s relations.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'sc_topic_requires_strength',
      'label' => 'Dependency strength',
      'required' => 1,
      'settings' => array(
        'max' => '',
        'min' => '',
        'prefix' => '',
        'suffix' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'number',
        'settings' => array(),
        'type' => 'number',
        'weight' => 0,
      ),
    ),
  );

  // Exported field: 'sc_topicrelation-isapartof-sc_topicrelation_strength'
  $fields['sc_topicrelation-isapartof-sc_topicrelation_strength'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'sc_topicrelation_strength',
      'field_permissions' => array(
        'type' => '2',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'vud_field',
      'settings' => array(
        'votingapi_tag' => 'vote',
        'vud_widget' => 'updown',
      ),
      'translatable' => '0',
      'type' => 'vud_field',
    ),
    'field_instance' => array(
      'bundle' => 'isapartof',
      'default_value' => array(
        0 => array(
          'vud_widget' => 'updown',
          'votingapi_tag' => 'vote',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'vud_field',
          'settings' => array(),
          'type' => 'default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'sc_topicrelation',
      'field_name' => 'sc_topicrelation_strength',
      'label' => 'Relation strength',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
        'votingapi_tag' => 'vote',
        'vud_widget' => 'mini',
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'vud_field',
        'settings' => array(),
        'type' => 'vud_field_default_widget',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'sc_topicrelation-requires-sc_topicrelation_strength'
  $fields['sc_topicrelation-requires-sc_topicrelation_strength'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'sc_topicrelation_strength',
      'field_permissions' => array(
        'type' => '2',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'vud_field',
      'settings' => array(
        'votingapi_tag' => 'vote',
        'vud_widget' => 'updown',
      ),
      'translatable' => '0',
      'type' => 'vud_field',
    ),
    'field_instance' => array(
      'bundle' => 'requires',
      'default_value' => array(
        0 => array(
          'vud_widget' => 'updown',
          'votingapi_tag' => 'vote',
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'vud_field',
          'settings' => array(),
          'type' => 'default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'sc_topicrelation',
      'field_name' => 'sc_topicrelation_strength',
      'label' => 'Relation strength',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
        'votingapi_tag' => 'vote',
        'vud_widget' => 'mini',
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'vud_field',
        'settings' => array(),
        'type' => 'vud_field_default_widget',
        'weight' => '2',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Dependency strength');
  t('Inclusion strength');
  t('Relation strength');
  t('This field should not be visible when editing topics – it is populated and changed by code to reflect votes on this topic\'s relations.');

  return $fields;
}
