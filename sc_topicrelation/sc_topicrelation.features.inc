<?php
/**
 * @file
 * sc_topicrelation.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sc_topicrelation_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function sc_topicrelation_eck_bundle_info() {
  $items = array(
  'sc_topicrelation_isapartof' => array(
  'machine_name' => 'sc_topicrelation_isapartof',
  'entity_type' => 'sc_topicrelation',
  'name' => 'isapartof',
  'label' => 'Is a part of',
),
  'sc_topicrelation_requires' => array(
  'machine_name' => 'sc_topicrelation_requires',
  'entity_type' => 'sc_topicrelation',
  'name' => 'requires',
  'label' => 'Requires',
),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function sc_topicrelation_eck_entity_type_info() {
$items = array(
       'sc_topicrelation' => array(
  'name' => 'sc_topicrelation',
  'label' => 'Topic relation',
  'properties' => array(
    'sid' => array(
      'label' => 'Source topic',
      'type' => 'positive_integer',
      'behavior' => '',
    ),
    'tid' => array(
      'label' => 'Target topic',
      'type' => 'positive_integer',
      'behavior' => '',
    ),
    'relation_strength_cache' => array(
      'label' => 'Relation strength cache',
      'type' => 'decimal',
      'behavior' => '',
    ),
  ),
),
  );
  return $items;
}
