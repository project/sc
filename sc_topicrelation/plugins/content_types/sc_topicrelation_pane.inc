<?php

/**
 * @file
 * Plugin declaration for 'add new topic relation' content pane, plus associated
 * functios.
 */

/**
 * Declaring the plugin.
 */
$plugin = array(
  'title' => t('Add new topic relation'),
  'single' => TRUE,
  'category' => array(t('Skill Compass'), -3),
  'edit form' => 'sc_topicrelation_pane_edit_form',
  'render callback' => 'sc_topicrelation_pane_render',
  'defaults' => array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'add_vote_new' => 1,
    'add_vote_existing' => 1,
    'button_text' => t('Create'),
  ),
  'cache' => TRUE,
  'admin title' => 'sc_topicrelation_pane_admin_title',
  'admin info' => 'sc_topicrelation_pane_admin_info',
  'required context' => new ctools_context_required(t('Relation start point'), 'node'),
);

/**
 * Implements hook_admin_title().
 */
function sc_topicrelation_pane_admin_title($subtype, $conf, $context = NULL) {
  $relation_info = sc_topics_relation_info();
  return t('Add new topic relation: @relation_type', array('@relation_type' => $relation_info[$conf['relation_type']]['label']));
}

/**
 * Implements hook_admin_info().
 */
function sc_topicrelation_pane_admin_info($subtype, $conf, $context = NULL) {
  // Get some data that will be handy later.
  $topic = &$context[$conf['context']];
  $yesno = array('0' => t('No'), '1' => t('Yes'));

  $info = new stdClass;
  $info->title = t('Added to: @topic', array('@topic' => $topic->identifier));
  $info->content = t('Vote added to new relation: @status', array('@status' => $yesno[$conf['add_vote_new']]));
  $info->content .= '<br />';
  $info->content .= t('Vote added to existing relation: @status', array('@status' => $yesno[$conf['add_vote_existing']]));
  $info->content .= '<br />';
  $info->content .= t('Button text: @button_text', array('@button_text' => $conf['button_text']));
  return $info;
}

/**
 * Admin edit form for the content pane.
 */
function sc_topicrelation_pane_edit_form($form, $form_state) {
  $conf = $form_state['conf'];

  $relation_info = sc_topics_relation_info();
  foreach ($relation_info as $key => $value) {
    $relation_types[$key] = $value['label'];
  }
  $form['relation_type'] = array(
    '#type' => 'select',
    '#title' => t('Relation type to create'),
    '#options' => $relation_types,
    '#default_value' => $conf['relation_type'],
  );
  $form['add_vote_new'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add a +1 vote from acting user to the new relation.'),
    '#default_value' => $conf['add_vote_new'],
  );
  $form['add_vote_existing'] = array(
    '#type' => 'checkbox',
    '#title' => t('If relation exists, add +1 vote from acting user.'),
    '#default_value' => $conf['add_vote_existing'],
  );
  $form['button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit button text'),
    '#required' => TRUE,
    '#default_value' => $conf['button_text'],
  );

  return $form;
}

/**
 * Config submit function, to store form values in pane configuration.
 */
function sc_topicrelation_pane_edit_form_submit(&$form, &$form_state) {
  $form_state['conf']['relation_type'] = $form_state['values']['relation_type'];
  $form_state['conf']['add_vote_new'] = $form_state['values']['add_vote_new'];
  $form_state['conf']['add_vote_existing'] = $form_state['values']['add_vote_existing'];
  $form_state['conf']['button_text'] = $form_state['values']['button_text'];
}

/**
 * Renders the pane to create new topic relations.
 *
 * @param type $subtype
 * @param type $conf
 * @param type $args
 * @param type $context
 * @return string
 */
function sc_topicrelation_pane_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $relation_type = &$conf['relation_type'];
  $relation_info = sc_topics_relation_info();
  $block->title = t('Add new "@relation_type" relation', array('@relation_type' => $relation_info[$relation_type]['label']));
  $block->content = drupal_get_form('sc_topicrelation_pane_render_form', $conf, $context->data->nid);
  return $block;
}

/**
 * Form builder function for the outward-facing form to add relations.
 *
 * @param $relation_type
 *   A string with the relation type, either 'isapartof' or 'requires'.
 * @param type $start_point
 *   The NID for the relation's starting point.
 * @return type
 *   The form array.
 */
function sc_topicrelation_pane_render_form($form, $form_state) {
  $conf = &$form_state['build_info']['args'][0];
  $source_nid = &$form_state['build_info']['args'][1];
  $relation_type = &$conf['relation_type'];
  $relation_info = sc_topics_relation_info();

  // Add all the config information that is needed later on in the form submit.
  $form['relation_type'] = array(
    '#type' => 'hidden',
    '#value' => $relation_type,
  );
  $form['source_nid'] = array(
    '#type' => 'hidden',
    '#value' => $source_nid,
  );
  $form['add_vote_new'] = array(
    '#type' => 'hidden',
    '#value' => $conf['add_vote_new'],
  );
  $form['add_vote_existing'] = array(
    '#type' => 'hidden',
    '#value' => $conf['add_vote_existing'],
  );
  $form['target'] = array(
    '#type' => 'textfield',
    '#title' => $relation_info[$relation_type]['label'],
    '#title_display' => 'invisible',
    '#required' => TRUE,
    '#size' => '24',
    '#autocomplete_path' => 'sc_topicrelation/autocomplete/' . $relation_type . '/' . $source_nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t($conf['button_text']),
  );

  return $form;
}

/**
 * Submit function for the outward-facing form, creating the actual relation.
 */
function sc_topicrelation_pane_render_form_submit($form, &$form_state) {
  // Get references to useful data, for easier handling.
  $source_nid = &$form_state['input']['source_nid'];
  $relation_type = &$form_state['input']['relation_type'];

  // Process the value for the target topic, given as 'title [nid:123]'.
  $target_info = explode(':', $form_state['input']['target']);
  // Just in case there is a colon in the topic title, we pop the last element
  // instead of selecting $target_info[1]. Cast to int to get rid of trailing
  // bracket.
  $target_nid = (int) array_pop($target_info);

  // Verify that we have sane IDs for source and target.
  if (count(entity_load('node', array($source_nid, $target_nid))) != 2) {
    watchdog('sc_topicrelation', t('Failed to create a relation with source @source and target @target: Missing end points.',
      array('@source' => $source_nid, '@target' => $target_nid)));
    drupal_set_message(t('Cannot create new relation: source or target is missing.'), 'error');
    return;;
  }

  // Check if there already exists a relation of this type between the topics.
  $existing_relation = new EntityFieldQuery();
  $existing_relation
    ->entityCondition('entity_type', 'sc_topicrelation')
    ->entityCondition('bundle', $relation_type)
    ->propertyCondition('sid', $source_nid)
    ->propertyCondition('tid', $target_nid)
    ->execute();

  if (isset($existing_relation->ordered_results) && count($existing_relation->ordered_results)) {
    // Do things to existing relation.
    drupal_set_message(t('The relation already exists.'));
    if ($form_state['input']['add_vote_existing']) {
      $new_vote = array(0 => array(
        'entity_type' => 'sc_topicrelation',
        'entity_id' => $existing_relation->ordered_results[0]->entity_id,
        'value_type' => 'points',
        'value' => 1,
      ));
      votingapi_set_votes($new_vote);
      drupal_set_message(t('Your vote was added to this relation.'));
    }
  }
  else {
    // Create new relation.
    $new_relation = entity_create('sc_topicrelation', array(
      'type' => $relation_type,
      'sid' => $source_nid,
      'tid' => $target_nid,
    ));
    $new_relation->save();

    // Add some messages, used only during development.
    drupal_set_message(t('The relation was created.'));

    if ($form_state['input']['add_vote_new']) {
      $new_vote = array(0 => array(
        'entity_type' => 'sc_topicrelation',
        'entity_id' => $new_relation->id,
        'value_type' => 'points',
        'value' => 1,
      ));
      votingapi_set_votes($new_vote);
      drupal_set_message(t('Your vote was added to the new relation'));
    }
  }
}
