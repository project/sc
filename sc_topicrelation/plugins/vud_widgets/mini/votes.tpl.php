<?php
/**
 * @file
 * votes.tpl.php
 *
 * Mini widget votes display for Vote Up/Down.
 */
?>
<span id="<?php print $id; ?>" class="total-votes-mini"><span class="<?php print $class; ?> total"><?php print $unsigned_points; ?> <?php print $vote_label; ?></span></span>
