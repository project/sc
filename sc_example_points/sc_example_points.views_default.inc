<?php
/**
 * @file
 * sc_example_points.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sc_example_points_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'sc_points_levels';
  $view->description = 'A description of the levels users can reach by collecting points.';
  $view->tag = 'default';
  $view->base_table = 'eck_levelup_level_limit';
  $view->human_name = 'Level up levels';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Belt levels';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Explanation text */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['ui_name'] = 'Explanation text';
  $handler->display->display_options['header']['area']['empty'] = FALSE;
  $handler->display->display_options['header']['area']['content'] = 'You get points for different actions and achievements. Here are descriptions of the levels you can reach with these points.';
  $handler->display->display_options['header']['area']['format'] = 'filtered_html';
  $handler->display->display_options['header']['area']['tokenize'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = '(no belt limits to display)';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Field: Level up limit: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_levelup_level_limit';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_type'] = 'h3';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  /* Field: Level up limit: Description */
  $handler->display->display_options['fields']['field_levelup_description']['id'] = 'field_levelup_description';
  $handler->display->display_options['fields']['field_levelup_description']['table'] = 'field_data_field_levelup_description';
  $handler->display->display_options['fields']['field_levelup_description']['field'] = 'field_levelup_description';
  $handler->display->display_options['fields']['field_levelup_description']['label'] = '';
  $handler->display->display_options['fields']['field_levelup_description']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_levelup_description']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_levelup_description']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_levelup_description']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_levelup_description']['field_api_classes'] = 0;

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'Belt levels';
  $handler->display->display_options['pane_description'] = 'A list of the Level Up limits set up on the site.';
  $handler->display->display_options['pane_category']['name'] = 'Skill Compass';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $export['sc_points_levels'] = $view;

  return $export;
}
