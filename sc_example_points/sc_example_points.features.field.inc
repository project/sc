<?php
/**
 * @file
 * sc_example_points.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function sc_example_points_field_default_fields() {
  $fields = array();

  // Exported field: 'levelup_level_limit-levelup_level_limit-field_levelup_description'
  $fields['levelup_level_limit-levelup_level_limit-field_levelup_description'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_levelup_description',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'levelup_level_limit',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'The description of this level, as displayed publicly on the site.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'levelup_level_limit',
      'field_name' => 'field_levelup_description',
      'label' => 'Description',
      'required' => 0,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '1',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Description');
  t('The description of this level, as displayed publicly on the site.');

  return $fields;
}
