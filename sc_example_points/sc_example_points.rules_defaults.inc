<?php
/**
 * @file
 * sc_example_points.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function sc_example_points_default_rules_configuration() {
  $items = array();
  $items['rules_sc_points_comment'] = entity_import('rules_config', '{ "rules_sc_points_comment" : {
      "LABEL" : "Get a point for each comment",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "comment" ],
      "ON" : [ "comment_insert" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "comment:author" ], "points" : "1" } }
      ]
    }
  }');
  $items['rules_sc_points_relation_create'] = entity_import('rules_config', '{ "rules_sc_points_relation_create" : {
      "LABEL" : "Get four points when creating new topic relations (plus one to topic authors)",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "eck" ],
      "ON" : [ "sc_topicrelation_insert" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "site:current-user" ], "points" : "4" } },
        { "component_rules_levelup_award_points" : { "account" : [ "sc-topicrelation:sid:author" ], "points" : "1" } },
        { "component_rules_levelup_award_points" : { "account" : [ "sc-topicrelation:tid:author" ], "points" : "1" } }
      ]
    }
  }');
  $items['rules_sc_points_relation_unvote'] = entity_import('rules_config', '{ "rules_sc_points_relation_unvote" : {
      "LABEL" : "Lose two points for unvoting on topic relations",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "voting_rules" ],
      "ON" : [ "voting_rules_delete_entity" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "vote:user" ], "points" : "-2" } }
      ]
    }
  }');
  $items['rules_sc_points_relation_vote'] = entity_import('rules_config', '{ "rules_sc_points_relation_vote" : {
      "LABEL" : "Get two points for voting on topic relations",
      "PLUGIN" : "reaction rule",
      "ACTIVE" : false,
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "voting_rules" ],
      "ON" : [ "voting_rules_insert_entity" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "vote:user" ], "points" : "2" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_complete'] = entity_import('rules_config', '{ "rules_sc_points_resource_complete" : {
      "LABEL" : "Get a point for completing a resource (plus one to author)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : [ "flag_flagged_sc_resource_completed" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "flagging-user" ], "points" : "1" } },
        { "component_rules_levelup_award_points" : { "account" : [ "flagged-node:author" ], "points" : "1" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_create'] = entity_import('rules_config', '{ "rules_sc_points_resource_create" : {
      "LABEL" : "Get 15 points for adding new resource (plus five to first topic owner)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "sc_resource_external" : "sc_resource_external" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "node:author" ], "points" : "15" } },
        { "component_rules_levelup_award_points" : { "account" : [ "node:sc-resource-topics:0:author" ], "points" : "5" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_drupalorg'] = entity_import('rules_config', '{ "rules_sc_points_resource_drupalorg" : {
      "LABEL" : "Two extra points if external resource points to drupal.org",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "sc_resource_external" : "sc_resource_external" } }
          }
        },
        { "text_matches" : {
            "text" : [ "node:sc-externalresource-link:url" ],
            "match" : "http:\\/\\/drupal.org\\/",
            "operation" : "starts"
          }
        }
      ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "node:author" ], "points" : "2" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_top_unvote'] = entity_import('rules_config', '{ "rules_sc_points_resource_top_unvote" : {
      "LABEL" : "Take two points from author when top vote on resource is removed",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "voting_rules", "rules" ],
      "ON" : [ "voting_rules_delete_node" ],
      "IF" : [
        { "voting_rules_condition_check_vote_value" : { "vote" : [ "vote" ], "value" : "100" } }
      ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "node:author" ], "points" : "-2" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_top_vote'] = entity_import('rules_config', '{ "rules_sc_points_resource_top_vote" : {
      "LABEL" : "Give author two points for top vote on resource",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "voting_rules", "rules" ],
      "ON" : [ "voting_rules_insert_node" ],
      "IF" : [
        { "voting_rules_condition_check_vote_value" : { "vote" : [ "vote" ], "value" : "100" } }
      ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "node:author" ], "points" : "2" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_unflag'] = entity_import('rules_config', '{ "rules_sc_points_resource_unflag" : {
      "LABEL" : "Lose a point for uncompleting a learning resource (plus one to author)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : [ "flag_unflagged_sc_resource_completed" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "flagging-user" ], "points" : "-1" } },
        { "component_rules_levelup_award_points" : { "account" : [ "flagged-node:author" ], "points" : "-1" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_unvote'] = entity_import('rules_config', '{ "rules_sc_points_resource_unvote" : {
      "LABEL" : "Lose two points for each resource vote removed",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "voting_rules" ],
      "ON" : [ "voting_rules_delete_node" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "sc_resource_external" : "sc_resource_external" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "vote:user" ], "points" : "-2" } }
      ]
    }
  }');
  $items['rules_sc_points_resource_vote'] = entity_import('rules_config', '{ "rules_sc_points_resource_vote" : {
      "LABEL" : "Get two points for each resource vote",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "voting_rules" ],
      "ON" : [ "voting_rules_insert_node" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "sc_resource_external" : "sc_resource_external" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "vote:user" ], "points" : "2" } }
      ]
    }
  }');
  $items['rules_sc_points_topic_create'] = entity_import('rules_config', '{ "rules_sc_points_topic_create" : {
      "LABEL" : "Get 15 points for adding new topic",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "sc_topic" : "sc_topic" } }
          }
        }
      ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "node:author" ], "points" : "15" } }
      ]
    }
  }');
  $items['rules_sc_points_topic_master'] = entity_import('rules_config', '{ "rules_sc_points_topic_master" : {
      "LABEL" : "Get five points for mastering a topic (plus one to author)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : [ "flag_flagged_sc_proficiency" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "flagging-user" ], "points" : "5" } },
        { "component_rules_levelup_award_points" : { "account" : [ "flagged-node:author" ], "points" : "1" } }
      ]
    }
  }');
  $items['rules_sc_points_topic_unmaster'] = entity_import('rules_config', '{ "rules_sc_points_topic_unmaster" : {
      "LABEL" : "Lose five points for unmastering a topic (plus one to author)",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : [ "flag_unflagged_sc_proficiency" ],
      "DO" : [
        { "component_rules_levelup_award_points" : { "account" : [ "flagging-user" ], "points" : "-5" } },
        { "component_rules_levelup_award_points" : { "account" : [ "flagged-node:author" ], "points" : "-1" } }
      ]
    }
  }');
  $items['rules_sc_points_whitebelt'] = entity_import('rules_config', '{ "rules_sc_points_whitebelt" : {
      "LABEL" : "Award white belt when account is created",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Skill Compass" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_insert" ],
      "DO" : [
        { "user_add_role" : { "account" : [ "account" ], "roles" : { "value" : { "5" : "' . user_role_load_by_name('white belt')->rid . '" } } } }
      ]
    }
  }');
  return $items;
}
