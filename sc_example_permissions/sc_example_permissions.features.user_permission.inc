<?php
/**
 * @file
 * sc_example_permissions.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function sc_example_permissions_user_default_permissions() {
  $permissions = array();

  // Exported permission: access comments
  $permissions['access comments'] = array(
    'name' => 'access comments',
    'roles' => array(
      0 => 'administrator',
      1 => 'anonymous user',
      2 => 'authenticated user',
    ),
    'module' => 'comment',
  );

  // Exported permission: access user profiles
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      0 => 'White belt',
      1 => 'administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: cancel account
  $permissions['cancel account'] = array(
    'name' => 'cancel account',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'user',
  );

  // Exported permission: create sc_resource_external content
  $permissions['create sc_resource_external content'] = array(
    'name' => 'create sc_resource_external content',
    'roles' => array(
      0 => 'Orange belt',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: create sc_topic content
  $permissions['create sc_topic content'] = array(
    'name' => 'create sc_topic content',
    'roles' => array(
      0 => 'Blue belt',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any sc_resource_external content
  $permissions['delete any sc_resource_external content'] = array(
    'name' => 'delete any sc_resource_external content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any sc_topic content
  $permissions['delete any sc_topic content'] = array(
    'name' => 'delete any sc_topic content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own sc_resource_external content
  $permissions['delete own sc_resource_external content'] = array(
    'name' => 'delete own sc_resource_external content',
    'roles' => array(
      0 => 'Orange belt',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own sc_topic content
  $permissions['delete own sc_topic content'] = array(
    'name' => 'delete own sc_topic content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any sc_resource_external content
  $permissions['edit any sc_resource_external content'] = array(
    'name' => 'edit any sc_resource_external content',
    'roles' => array(
      0 => 'Brown belt',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any sc_topic content
  $permissions['edit any sc_topic content'] = array(
    'name' => 'edit any sc_topic content',
    'roles' => array(
      0 => 'Brown belt',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own comments
  $permissions['edit own comments'] = array(
    'name' => 'edit own comments',
    'roles' => array(
      0 => 'Yellow belt',
      1 => 'administrator',
    ),
    'module' => 'comment',
  );

  // Exported permission: edit own sc_resource_external content
  $permissions['edit own sc_resource_external content'] = array(
    'name' => 'edit own sc_resource_external content',
    'roles' => array(
      0 => 'Orange belt',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own sc_topic content
  $permissions['edit own sc_topic content'] = array(
    'name' => 'edit own sc_topic content',
    'roles' => array(
      0 => 'Blue belt',
      1 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: post comments
  $permissions['post comments'] = array(
    'name' => 'post comments',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'comment',
  );

  // Exported permission: sc_proficiency
  $permissions['sc_proficiency'] = array(
    'name' => 'sc_proficiency',
    'roles' => array(
      0 => 'Yellow belt',
      1 => 'administrator',
    ),
    'module' => 'sc_example_permissions',
  );

  // Exported permission: sc_resource_completed
  $permissions['sc_resource_completed'] = array(
    'name' => 'sc_resource_completed',
    'roles' => array(
      0 => 'White belt',
      1 => 'administrator',
    ),
    'module' => 'sc_example_permissions',
  );

  // Exported permission: skip comment approval
  $permissions['skip comment approval'] = array(
    'name' => 'skip comment approval',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'comment',
  );

  // Exported permission: use vote up/down
  $permissions['use vote up/down'] = array(
    'name' => 'use vote up/down',
    'roles' => array(
      0 => 'Orange belt',
      1 => 'administrator',
    ),
    'module' => 'vud',
  );

  return $permissions;
}
