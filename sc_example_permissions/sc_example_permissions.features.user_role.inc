<?php
/**
 * @file
 * sc_example_permissions.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function sc_example_permissions_user_default_roles() {
  $roles = array();

  // Exported role: Blue belt
  $roles['Blue belt'] = array(
    'name' => 'Blue belt',
    'weight' => '8',
  );

  // Exported role: Brown belt
  $roles['Brown belt'] = array(
    'name' => 'Brown belt',
    'weight' => '9',
  );

  // Exported role: Green belt
  $roles['Green belt'] = array(
    'name' => 'Green belt',
    'weight' => '7',
  );

  // Exported role: Orange belt
  $roles['Orange belt'] = array(
    'name' => 'Orange belt',
    'weight' => '6',
  );

  // Exported role: White belt
  $roles['White belt'] = array(
    'name' => 'White belt',
    'weight' => '4',
  );

  // Exported role: Yellow belt
  $roles['Yellow belt'] = array(
    'name' => 'Yellow belt',
    'weight' => '5',
  );

  return $roles;
}
