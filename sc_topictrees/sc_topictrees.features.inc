<?php
/**
 * @file
 * sc_topictrees.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sc_topictrees_views_api() {
  return array("version" => "3.0");
}
