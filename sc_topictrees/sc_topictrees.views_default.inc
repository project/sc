<?php
/**
 * @file
 * sc_topictrees.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sc_topictrees_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'sc_topic_trees';
  $view->description = 'Inclusion and dependency trees for topics.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Topic trees';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Topics and sub topics';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'graphapi_style';
  $handler->display->display_options['style_options']['engine'] = 'thejit_spacetree';
  $handler->display->display_options['style_options']['mapping'] = array(
    'from' => array(
      'id' => 'nid_1',
      'label' => 'title_1',
      'uri' => 'nid_1',
    ),
    'to' => array(
      'id' => 'nid',
      'label' => 'title',
      'uri' => 'nid',
    ),
  );
  $handler->display->display_options['style_options']['thejit_spacetree'] = array(
    'duration' => '250',
    'orient' => array(
      'enable_orient' => 0,
      'init_orient' => 'bottom',
    ),
    'search' => array(
      'enable_search' => 0,
      'search_path' => 'jit/spacetree/debug/search',
    ),
    'edge_colors' => array(
      'edge_color' => '#ffffff',
      'selected_edge_color' => '#25aef5',
    ),
    'enable_full_screen' => 1,
    'enable_hiding' => 0,
    'help' => array(
      'enable_help' => 0,
      'help_function_name' => '_thejit_spacetree_debug_help',
    ),
    'node_info' => array(
      'enable_node_info' => 1,
      'node_info_path' => 'sc_topictrees/tree-info-callback',
      'cache_node_info' => 1,
    ),
  );
  /* Relationship: Larger topic */
  $handler->display->display_options['relationships']['sc_topic_isapartof_target_id']['id'] = 'sc_topic_isapartof_target_id';
  $handler->display->display_options['relationships']['sc_topic_isapartof_target_id']['table'] = 'field_data_sc_topic_isapartof';
  $handler->display->display_options['relationships']['sc_topic_isapartof_target_id']['field'] = 'sc_topic_isapartof_target_id';
  $handler->display->display_options['relationships']['sc_topic_isapartof_target_id']['ui_name'] = 'Larger topic';
  $handler->display->display_options['relationships']['sc_topic_isapartof_target_id']['label'] = 'Larger topic';
  $handler->display->display_options['relationships']['sc_topic_isapartof_target_id']['required'] = TRUE;
  /* Field: Topic NID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['ui_name'] = 'Topic NID';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Topic title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['ui_name'] = 'Topic title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Parent NID */
  $handler->display->display_options['fields']['nid_1']['id'] = 'nid_1';
  $handler->display->display_options['fields']['nid_1']['table'] = 'node';
  $handler->display->display_options['fields']['nid_1']['field'] = 'nid';
  $handler->display->display_options['fields']['nid_1']['relationship'] = 'sc_topic_isapartof_target_id';
  $handler->display->display_options['fields']['nid_1']['ui_name'] = 'Parent NID';
  $handler->display->display_options['fields']['nid_1']['label'] = '';
  $handler->display->display_options['fields']['nid_1']['element_label_colon'] = FALSE;
  /* Field: Parent title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'sc_topic_isapartof_target_id';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Parent title';
  $handler->display->display_options['fields']['title_1']['label'] = '';
  $handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title_1']['link_to_node'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'sc_topic' => 'sc_topic',
  );

  /* Display: Is a part of */
  $handler = $view->new_display('panel_pane', 'Is a part of', 'panel_pane_1');
  $handler->display->display_options['display_description'] = 'A tree describing topics and their sub topics.';
  $handler->display->display_options['pane_title'] = 'Sub topic tree';
  $handler->display->display_options['pane_description'] = 'A graphical representation of strongest "is a part of" relations between topics.';
  $handler->display->display_options['pane_category']['name'] = 'Skill Compass';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 1,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Super topic (if any)',
    ),
  );

  /* Display: Requires */
  $handler = $view->new_display('panel_pane', 'Requires', 'panel_pane_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Topic dependencies';
  $handler->display->display_options['display_description'] = 'A tree describing topics and the topics they lead to.';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'graphapi_style';
  $handler->display->display_options['style_options']['engine'] = 'thejit_spacetree';
  $handler->display->display_options['style_options']['mapping'] = array(
    'from' => array(
      'id' => 'nid_1',
      'label' => 'title_1',
      'uri' => 'nid_1',
    ),
    'to' => array(
      'id' => 'nid',
      'label' => 'title',
      'uri' => 'nid',
    ),
  );
  $handler->display->display_options['style_options']['thejit_spacetree'] = array(
    'duration' => '250',
    'orient' => array(
      'enable_orient' => 0,
      'init_orient' => 'left',
    ),
    'search' => array(
      'enable_search' => 0,
      'search_path' => 'jit/spacetree/debug/search',
    ),
    'edge_colors' => array(
      'edge_color' => '#ffffff',
      'selected_edge_color' => '#25aef5',
    ),
    'enable_full_screen' => 1,
    'enable_hiding' => 0,
    'help' => array(
      'enable_help' => 0,
      'help_function_name' => '_thejit_spacetree_debug_help',
    ),
    'node_info' => array(
      'enable_node_info' => 1,
      'node_info_path' => 'sc_topictrees/tree-info-callback',
      'cache_node_info' => 1,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Required topic */
  $handler->display->display_options['relationships']['sc_topic_requires_target_id']['id'] = 'sc_topic_requires_target_id';
  $handler->display->display_options['relationships']['sc_topic_requires_target_id']['table'] = 'field_data_sc_topic_requires';
  $handler->display->display_options['relationships']['sc_topic_requires_target_id']['field'] = 'sc_topic_requires_target_id';
  $handler->display->display_options['relationships']['sc_topic_requires_target_id']['ui_name'] = 'Required topic';
  $handler->display->display_options['relationships']['sc_topic_requires_target_id']['label'] = 'Required topic';
  $handler->display->display_options['relationships']['sc_topic_requires_target_id']['required'] = TRUE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Topic NID */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['ui_name'] = 'Topic NID';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Topic title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['ui_name'] = 'Topic title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Parent NID */
  $handler->display->display_options['fields']['nid_1']['id'] = 'nid_1';
  $handler->display->display_options['fields']['nid_1']['table'] = 'node';
  $handler->display->display_options['fields']['nid_1']['field'] = 'nid';
  $handler->display->display_options['fields']['nid_1']['relationship'] = 'sc_topic_requires_target_id';
  $handler->display->display_options['fields']['nid_1']['ui_name'] = 'Parent NID';
  $handler->display->display_options['fields']['nid_1']['label'] = '';
  $handler->display->display_options['fields']['nid_1']['element_label_colon'] = FALSE;
  /* Field: Parent title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'node';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'sc_topic_requires_target_id';
  $handler->display->display_options['fields']['title_1']['ui_name'] = 'Parent title';
  $handler->display->display_options['fields']['title_1']['label'] = '';
  $handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title_1']['link_to_node'] = FALSE;
  $handler->display->display_options['pane_title'] = 'Topics dependency tree';
  $handler->display->display_options['pane_description'] = 'A graphical representation of strongest "requires" relations between topics.';
  $handler->display->display_options['pane_category']['name'] = 'Skill Compass';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 1,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Starting topic (if any)',
    ),
  );
  $export['sc_topic_trees'] = $view;

  $view = new view;
  $view->name = 'sc_topictree_item_info';
  $view->description = 'A view returning info about a selected item in a topic tree. Only called from code! See learnsite_tree_graphs.module.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Topic tree item info';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Title element */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['ui_name'] = 'Title element';
  $handler->display->display_options['fields']['title']['label'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Item description */
  $handler->display->display_options['fields']['sc_topic_description']['id'] = 'sc_topic_description';
  $handler->display->display_options['fields']['sc_topic_description']['table'] = 'field_data_sc_topic_description';
  $handler->display->display_options['fields']['sc_topic_description']['field'] = 'sc_topic_description';
  $handler->display->display_options['fields']['sc_topic_description']['ui_name'] = 'Item description';
  $handler->display->display_options['fields']['sc_topic_description']['label'] = 'body';
  $handler->display->display_options['fields']['sc_topic_description']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['sc_topic_description']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['sc_topic_description']['settings'] = array(
    'trim_length' => '200',
  );
  /* Contextual filter: Item NID */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['ui_name'] = 'Item NID';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  $export['sc_topictree_item_info'] = $view;

  return $export;
}
