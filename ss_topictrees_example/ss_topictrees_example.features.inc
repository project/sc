<?php
/**
 * @file
 * ss_topictrees_example.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ss_topictrees_example_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
