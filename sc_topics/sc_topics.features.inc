<?php
/**
 * @file
 * sc_topics.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sc_topics_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function sc_topics_node_info() {
  $items = array(
    'sc_topic' => array(
      'name' => t('Topic'),
      'base' => 'node_content',
      'description' => t('Topics are used to break down learning areas into managable chunks. A topic may be a part of another topic, or require that another topic is learnt first. Depending on the site setup, topics may also have tutorials and other resources attached to them.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
