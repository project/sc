<?php
/**
 * @file
 * sc_resource_external.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sc_resource_external_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'sc_learning_resources';
  $view->description = 'Lists of learing resources.';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Learning resources';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Learning resources';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['distinct'] = TRUE;
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Show results';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'sc_resource_topics' => 'sc_resource_topics',
    'type' => 'title',
    'value' => 'value',
    'value_1' => 'value_1',
    'value_2' => 'value_2',
    'ops' => 'ops',
  );
  $handler->display->display_options['style_options']['default'] = 'value';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '<br />',
      'empty_column' => 0,
    ),
    'sc_resource_topics' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'type' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value_1' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'value_2' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'ops' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* No results behavior: No results text */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['ui_name'] = 'No results text';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no learning resources to show. :-(';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Flags: sc_resource_completed */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['ui_name'] = 'Completed flag';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Completed';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'sc_resource_completed';
  /* Relationship: Topics */
  $handler->display->display_options['relationships']['sc_resource_topics_target_id']['id'] = 'sc_resource_topics_target_id';
  $handler->display->display_options['relationships']['sc_resource_topics_target_id']['table'] = 'field_data_sc_resource_topics';
  $handler->display->display_options['relationships']['sc_resource_topics_target_id']['field'] = 'sc_resource_topics_target_id';
  $handler->display->display_options['relationships']['sc_resource_topics_target_id']['ui_name'] = 'Topics';
  $handler->display->display_options['relationships']['sc_resource_topics_target_id']['label'] = 'Topics';
  /* Relationship: Correct votes */
  $handler->display->display_options['relationships']['votingapi_cache']['id'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_cache']['field'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache']['ui_name'] = 'Correct votes';
  $handler->display->display_options['relationships']['votingapi_cache']['label'] = 'Correct votes';
  $handler->display->display_options['relationships']['votingapi_cache']['required'] = 0;
  $handler->display->display_options['relationships']['votingapi_cache']['votingapi'] = array(
    'value_type' => 'percent',
    'tag' => 'correct',
    'function' => 'average',
  );
  /* Relationship: Clear votes */
  $handler->display->display_options['relationships']['votingapi_cache_1']['id'] = 'votingapi_cache_1';
  $handler->display->display_options['relationships']['votingapi_cache_1']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_cache_1']['field'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache_1']['ui_name'] = 'Clear votes';
  $handler->display->display_options['relationships']['votingapi_cache_1']['label'] = 'Clear votes';
  $handler->display->display_options['relationships']['votingapi_cache_1']['required'] = 0;
  $handler->display->display_options['relationships']['votingapi_cache_1']['votingapi'] = array(
    'value_type' => 'percent',
    'tag' => 'clear',
    'function' => 'average',
  );
  /* Relationship: Entertaining votes */
  $handler->display->display_options['relationships']['votingapi_cache_2']['id'] = 'votingapi_cache_2';
  $handler->display->display_options['relationships']['votingapi_cache_2']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_cache_2']['field'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache_2']['ui_name'] = 'Entertaining votes';
  $handler->display->display_options['relationships']['votingapi_cache_2']['label'] = 'Entertaining votes';
  $handler->display->display_options['relationships']['votingapi_cache_2']['required'] = 0;
  $handler->display->display_options['relationships']['votingapi_cache_2']['votingapi'] = array(
    'value_type' => 'percent',
    'tag' => 'entertaining',
    'function' => 'average',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Learning resource';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Topics */
  $handler->display->display_options['fields']['sc_resource_topics']['id'] = 'sc_resource_topics';
  $handler->display->display_options['fields']['sc_resource_topics']['table'] = 'field_data_sc_resource_topics';
  $handler->display->display_options['fields']['sc_resource_topics']['field'] = 'sc_resource_topics';
  $handler->display->display_options['fields']['sc_resource_topics']['settings'] = array(
    'link' => 1,
  );
  $handler->display->display_options['fields']['sc_resource_topics']['delta_offset'] = '0';
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['type']['alter']['text'] = '([type])';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Vote results: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['relationship'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value']['label'] = 'Correct';
  $handler->display->display_options['fields']['value']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['value']['alter']['text'] = '[value]%';
  $handler->display->display_options['fields']['value']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['value']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['value']['alter']['external'] = 0;
  $handler->display->display_options['fields']['value']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['value']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['value']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['value']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['value']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['value']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['value']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['value']['alter']['html'] = 0;
  $handler->display->display_options['fields']['value']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['value']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['value']['hide_empty'] = 0;
  $handler->display->display_options['fields']['value']['empty_zero'] = 0;
  $handler->display->display_options['fields']['value']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['value']['set_precision'] = 0;
  $handler->display->display_options['fields']['value']['precision'] = '0';
  $handler->display->display_options['fields']['value']['format_plural'] = 0;
  /* Field: Vote results: Value */
  $handler->display->display_options['fields']['value_1']['id'] = 'value_1';
  $handler->display->display_options['fields']['value_1']['table'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value_1']['field'] = 'value';
  $handler->display->display_options['fields']['value_1']['relationship'] = 'votingapi_cache_1';
  $handler->display->display_options['fields']['value_1']['label'] = 'Clear';
  $handler->display->display_options['fields']['value_1']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['value_1']['alter']['text'] = '[value_1]%';
  $handler->display->display_options['fields']['value_1']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['external'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['value_1']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['value_1']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['value_1']['alter']['html'] = 0;
  $handler->display->display_options['fields']['value_1']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['value_1']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['value_1']['hide_empty'] = 0;
  $handler->display->display_options['fields']['value_1']['empty_zero'] = 0;
  $handler->display->display_options['fields']['value_1']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['value_1']['set_precision'] = 0;
  $handler->display->display_options['fields']['value_1']['precision'] = '0';
  $handler->display->display_options['fields']['value_1']['format_plural'] = 0;
  /* Field: Vote results: Value */
  $handler->display->display_options['fields']['value_2']['id'] = 'value_2';
  $handler->display->display_options['fields']['value_2']['table'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value_2']['field'] = 'value';
  $handler->display->display_options['fields']['value_2']['relationship'] = 'votingapi_cache_2';
  $handler->display->display_options['fields']['value_2']['label'] = 'Entertaining';
  $handler->display->display_options['fields']['value_2']['alter']['alter_text'] = 1;
  $handler->display->display_options['fields']['value_2']['alter']['text'] = '[value_2]%';
  $handler->display->display_options['fields']['value_2']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['value_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['value_2']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['value_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['value_2']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['value_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['value_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['value_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['value_2']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['value_2']['set_precision'] = 0;
  $handler->display->display_options['fields']['value_2']['precision'] = '0';
  $handler->display->display_options['fields']['value_2']['format_plural'] = 0;
  /* Field: Completed flag */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flag_content';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['ui_name'] = 'Completed flag';
  $handler->display->display_options['fields']['ops']['label'] = 'Status';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Has the resource-topic reference */
  $handler->display->display_options['filters']['sc_resource_topics_target_id']['id'] = 'sc_resource_topics_target_id';
  $handler->display->display_options['filters']['sc_resource_topics_target_id']['table'] = 'field_data_sc_resource_topics';
  $handler->display->display_options['filters']['sc_resource_topics_target_id']['field'] = 'sc_resource_topics_target_id';
  $handler->display->display_options['filters']['sc_resource_topics_target_id']['ui_name'] = 'Has the resource-topic reference';
  $handler->display->display_options['filters']['sc_resource_topics_target_id']['operator'] = 'not empty';
  $handler->display->display_options['filters']['sc_resource_topics_target_id']['group'] = 1;
  /* Filter criterion: Search by title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['ui_name'] = 'Search by title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['group'] = 1;
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Search by title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  /* Filter criterion: Search by topic */
  $handler->display->display_options['filters']['title_1']['id'] = 'title_1';
  $handler->display->display_options['filters']['title_1']['table'] = 'node';
  $handler->display->display_options['filters']['title_1']['field'] = 'title';
  $handler->display->display_options['filters']['title_1']['relationship'] = 'sc_resource_topics_target_id';
  $handler->display->display_options['filters']['title_1']['ui_name'] = 'Search by topic';
  $handler->display->display_options['filters']['title_1']['operator'] = 'contains';
  $handler->display->display_options['filters']['title_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title_1']['expose']['operator_id'] = 'title_1_op';
  $handler->display->display_options['filters']['title_1']['expose']['label'] = 'Search by topic';
  $handler->display->display_options['filters']['title_1']['expose']['operator'] = 'title_1_op';
  $handler->display->display_options['filters']['title_1']['expose']['identifier'] = 'topic';
  /* Filter criterion: Filter by completion */
  $handler->display->display_options['filters']['flagged']['id'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['table'] = 'flag_content';
  $handler->display->display_options['filters']['flagged']['field'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['filters']['flagged']['ui_name'] = 'Filter by completion';
  $handler->display->display_options['filters']['flagged']['value'] = 'All';
  $handler->display->display_options['filters']['flagged']['exposed'] = TRUE;
  $handler->display->display_options['filters']['flagged']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['flagged']['expose']['label'] = 'Filter by status';
  $handler->display->display_options['filters']['flagged']['expose']['operator'] = 'flagged_op';
  $handler->display->display_options['filters']['flagged']['expose']['identifier'] = 'completed';

  /* Display: Browse all resources */
  $handler = $view->new_display('panel_pane', 'Browse all resources', 'panel_pane_1');
  $handler->display->display_options['pane_title'] = 'Learning resources';
  $handler->display->display_options['pane_description'] = 'Lists learning resources on the site.';
  $handler->display->display_options['pane_category']['name'] = 'Skill Compass';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 'fields_override';
  $handler->display->display_options['argument_input'] = array(
    'sc_resource_topics_target_id' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 1,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Topic (if any)',
    ),
    'nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 1,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Learning resource to exclude',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Resources per topic */
  $handler = $view->new_display('panel_pane', 'Resources per topic', 'panel_pane_2');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Flags: sc_resource_completed */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['ui_name'] = 'Completed flag';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Completed';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'sc_resource_completed';
  /* Relationship: Correct votes */
  $handler->display->display_options['relationships']['votingapi_cache']['id'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_cache']['field'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache']['ui_name'] = 'Correct votes';
  $handler->display->display_options['relationships']['votingapi_cache']['label'] = 'Correct votes';
  $handler->display->display_options['relationships']['votingapi_cache']['votingapi'] = array(
    'value_type' => 'percent',
    'tag' => 'correct',
    'function' => 'average',
  );
  /* Relationship: Clear votes */
  $handler->display->display_options['relationships']['votingapi_cache_1']['id'] = 'votingapi_cache_1';
  $handler->display->display_options['relationships']['votingapi_cache_1']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_cache_1']['field'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache_1']['ui_name'] = 'Clear votes';
  $handler->display->display_options['relationships']['votingapi_cache_1']['label'] = 'Clear votes';
  $handler->display->display_options['relationships']['votingapi_cache_1']['votingapi'] = array(
    'value_type' => 'percent',
    'tag' => 'clear',
    'function' => 'average',
  );
  /* Relationship: Entertaining votes */
  $handler->display->display_options['relationships']['votingapi_cache_2']['id'] = 'votingapi_cache_2';
  $handler->display->display_options['relationships']['votingapi_cache_2']['table'] = 'node';
  $handler->display->display_options['relationships']['votingapi_cache_2']['field'] = 'votingapi_cache';
  $handler->display->display_options['relationships']['votingapi_cache_2']['ui_name'] = 'Entertaining votes';
  $handler->display->display_options['relationships']['votingapi_cache_2']['label'] = 'Entertaining votes';
  $handler->display->display_options['relationships']['votingapi_cache_2']['votingapi'] = array(
    'value_type' => 'percent',
    'tag' => 'entertaining',
    'function' => 'average',
  );
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Learning resource';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'node';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['label'] = '';
  $handler->display->display_options['fields']['type']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['type']['alter']['text'] = '([type])';
  $handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
  /* Field: Vote results: Value */
  $handler->display->display_options['fields']['value']['id'] = 'value';
  $handler->display->display_options['fields']['value']['table'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value']['field'] = 'value';
  $handler->display->display_options['fields']['value']['relationship'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value']['label'] = 'Correct';
  $handler->display->display_options['fields']['value']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['value']['alter']['text'] = '[value]%';
  $handler->display->display_options['fields']['value']['precision'] = '0';
  /* Field: Vote results: Value */
  $handler->display->display_options['fields']['value_1']['id'] = 'value_1';
  $handler->display->display_options['fields']['value_1']['table'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value_1']['field'] = 'value';
  $handler->display->display_options['fields']['value_1']['relationship'] = 'votingapi_cache_1';
  $handler->display->display_options['fields']['value_1']['label'] = 'Clear';
  $handler->display->display_options['fields']['value_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['value_1']['alter']['text'] = '[value_1]%';
  $handler->display->display_options['fields']['value_1']['precision'] = '0';
  /* Field: Vote results: Value */
  $handler->display->display_options['fields']['value_2']['id'] = 'value_2';
  $handler->display->display_options['fields']['value_2']['table'] = 'votingapi_cache';
  $handler->display->display_options['fields']['value_2']['field'] = 'value';
  $handler->display->display_options['fields']['value_2']['relationship'] = 'votingapi_cache_2';
  $handler->display->display_options['fields']['value_2']['label'] = 'Entertaining';
  $handler->display->display_options['fields']['value_2']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['value_2']['alter']['text'] = '[value_2]%';
  $handler->display->display_options['fields']['value_2']['precision'] = '0';
  /* Field: Completed flag */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flag_content';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['ui_name'] = 'Completed flag';
  $handler->display->display_options['fields']['ops']['label'] = 'Status';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Topic */
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['id'] = 'sc_resource_topics_target_id';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['table'] = 'field_data_sc_resource_topics';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['field'] = 'sc_resource_topics_target_id';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['ui_name'] = 'Topic';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['title'] = 'Resources for learning %1';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['sc_resource_topics_target_id']['validate_options']['types'] = array(
    'sc_topic' => 'sc_topic',
  );
  /* Contextual filter: Learning resource to exclude */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['ui_name'] = 'Learning resource to exclude';
  $handler->display->display_options['arguments']['nid']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['nid']['title'] = 'Other resources on this topic';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['nid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['nid']['validate']['type'] = 'node';
  $handler->display->display_options['arguments']['nid']['not'] = TRUE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  $handler->display->display_options['pane_title'] = 'Learning resources for a topic';
  $handler->display->display_options['pane_description'] = 'Lists learning resources for a given topic.';
  $handler->display->display_options['pane_category']['name'] = 'Skill Compass';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['allow']['use_pager'] = 'use_pager';
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 'fields_override';
  $handler->display->display_options['argument_input'] = array(
    'sc_resource_topics_target_id' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 0,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Topic (if any)',
    ),
    'nid' => array(
      'type' => 'context',
      'context' => 'entity:node.nid',
      'context_optional' => 1,
      'panel' => '0',
      'fixed' => '',
      'label' => 'Learning resource to exclude',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';
  $export['sc_learning_resources'] = $view;

  return $export;
}
