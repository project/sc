<?php
/**
 * @file
 * sc_resource_external.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sc_resource_external_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function sc_resource_external_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function sc_resource_external_flag_default_flags() {
  $flags = array();
  // Exported flag: "Resource completed".
  $flags['sc_resource_completed'] = array(
    'content_type' => 'node',
    'title' => 'Resource completed',
    'global' => '0',
    'types' => array(
      0 => 'sc_resource_external',
    ),
    'flag_short' => 'Mark completed',
    'flag_long' => 'Click here to note that you have worked your way through this learning resource.',
    'flag_message' => 'Way to go!',
    'unflag_short' => 'Completed!',
    'unflag_long' => 'Click here to no longer mark this resource as completed.',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'sc_resource_external',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}

/**
 * Implements hook_node_info().
 */
function sc_resource_external_node_info() {
  $items = array(
    'sc_resource_external' => array(
      'name' => t('External learning resource'),
      'base' => 'node_content',
      'description' => t('External learning resources only contain a description and a reference to an external page. As all learning resources, they should relate to one or more topics.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
