<?php
/**
 * @file
 * sc_topic_proficiency.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sc_topic_proficiency_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function sc_topic_proficiency_flag_default_flags() {
  $flags = array();
  // Exported flag: "Topic mastered".
  $flags['sc_proficiency'] = array(
    'content_type' => 'node',
    'title' => 'Topic mastered',
    'global' => '0',
    'types' => array(
      0 => 'sc_topic',
    ),
    'flag_short' => 'Mark learned',
    'flag_long' => 'Click here to note that you are proficient in this topic.',
    'flag_message' => 'Way to go!',
    'unflag_short' => 'Learned!',
    'unflag_long' => 'Click here if you are no longer proficient in the topic "[node:title]".',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'i18n' => 0,
    'module' => 'sc_topic_proficiency',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;
}
